package com.application.socialnetworkapp.repository;


import lombok.RequiredArgsConstructor;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@RequiredArgsConstructor
class UserRepositoryTest {

    @Autowired
    UserRepository userRepositoryTest;

    @Test
    void itShouldFindByExistUsername() {
        String username = "adminapp";
        var exists = userRepositoryTest.findByExistUsername(username);
        assertThat(exists).isTrue();
    }

    @Test
    void itShouldFindByExistUsernameTwo() {
        String username = "adminapp";
        var exists = userRepositoryTest.findByExistUsername(username);
        assertThat(exists).isTrue();
    }
}