package com.application.socialnetworkapp.service.auth;

import com.application.socialnetworkapp.dto.request.user.LoginDto;
import com.application.socialnetworkapp.dto.request.user.RegisterDto;
import org.springframework.http.ResponseEntity;

public interface AuthService {
    ResponseEntity<Object> register(RegisterDto request);
    ResponseEntity<Object> login(LoginDto request);
}
