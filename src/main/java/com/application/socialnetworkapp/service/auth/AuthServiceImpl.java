package com.application.socialnetworkapp.service.auth;

import com.application.socialnetworkapp.dto.request.user.LoginDto;
import com.application.socialnetworkapp.dto.request.user.RegisterDto;
import com.application.socialnetworkapp.dto.response.user.SignInResponseDto;
import com.application.socialnetworkapp.entity.User;
import com.application.socialnetworkapp.exception.response.ResponseHandler;
import com.application.socialnetworkapp.repository.UserRepository;
import com.application.socialnetworkapp.security.jwt.JwtService;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class AuthServiceImpl implements AuthService {
    private final ModelMapper modelMapper = new ModelMapper();
    private final UserRepository userRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    private final AuthenticationManager authenticationManager;


    @Autowired
    JwtService jwtService;


    @Transactional
    @Override
    public ResponseEntity<Object> register(RegisterDto request) {
        User userMapper = modelMapper.map(request, User.class);
        userMapper.setPassword(passwordEncoder.encode(request.getPassword()));
        var data = userRepository.save(userMapper);
        return ResponseHandler.generateResponseSuccess(data);
    }

    @Transactional
    @Override
    public ResponseEntity<Object> login(LoginDto request) {
        Authentication authentication = authenticationManager
                .authenticate(new UsernamePasswordAuthenticationToken(request.getUsername(), request.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);

        var data  = userRepository.findByUsername(request.getUsername()).orElseThrow();
        var token = jwtService.generateToken(data);

        SignInResponseDto response = new SignInResponseDto(data.getUsername(),data.getRole().name(),token);
        return ResponseHandler.generateResponseSuccess(response);
    }

}
