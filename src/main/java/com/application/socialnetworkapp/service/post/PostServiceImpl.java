package com.application.socialnetworkapp.service.post;

import com.application.socialnetworkapp.dto.request.post.AddPostRequest;
import com.application.socialnetworkapp.dto.request.post.PostPageReq;
import com.application.socialnetworkapp.entity.Post;
import com.application.socialnetworkapp.exception.response.ResponseHandler;
import com.application.socialnetworkapp.repository.PostRepository;
import com.application.socialnetworkapp.util.UserAuth;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.sql.rowset.serial.SerialBlob;
import java.sql.Blob;
import java.util.Base64;

@Service
@RequiredArgsConstructor
public class PostServiceImpl implements PostService {
    private final ModelMapper modelMapper = new ModelMapper();
    private final PostRepository postRepository;
    private final UserAuth userAuth;

    public Blob convertBase64ToBlob(String base64String) {
        try {
            byte[] bytes = Base64.getDecoder().decode(base64String);
            return new SerialBlob(bytes);
        } catch (java.sql.SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Transactional
    @Override
    public ResponseEntity<Object> createPost(AddPostRequest request) {
        Post postMapper = modelMapper.map(request, Post.class);
        byte[] photoBytes = Base64.getDecoder().decode(request.getPhoto());
        postMapper.setUser(userAuth.getUserLogin());
        postMapper.setPhoto(photoBytes);
        Post savedPost = postRepository.save(postMapper);
        return ResponseHandler.generateResponseSuccess(savedPost);
    }


    public ResponseEntity<Object> listPostUser(PostPageReq req) {
        PageRequest pageable = PageRequest.of(req.getOffset(), req.getLimit());
        var res = postRepository.findAllByUser(userAuth.getUserLogin(), pageable);
        return ResponseHandler.generateResponseSuccess(res);
    }


}
