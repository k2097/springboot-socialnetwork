package com.application.socialnetworkapp.service.post;

import com.application.socialnetworkapp.dto.request.post.AddPostRequest;
import org.springframework.http.ResponseEntity;

public interface PostService {
    ResponseEntity<Object> createPost(AddPostRequest request);
}
