package com.application.socialnetworkapp.repository;

import com.application.socialnetworkapp.entity.Post;
import com.application.socialnetworkapp.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface PostRepository extends JpaRepository<Post,String> {

    Page<Post> findAllByUser(User user, Pageable pageable);

}
