package com.application.socialnetworkapp.repository;

import com.application.socialnetworkapp.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User,String> {
   Optional<User> findByUsername(@Param("username") String username);

   @Query("SELECT CASE WHEN COUNT(u) > 0 THEN TRUE ELSE FALSE END " +
           "FROM User u " +
           "WHERE u.username = :username"
   )
   boolean findByExistUsername(String username);

}
