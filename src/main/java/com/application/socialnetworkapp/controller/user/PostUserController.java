package com.application.socialnetworkapp.controller.user;

import com.application.socialnetworkapp.dto.request.post.AddPostRequest;
import com.application.socialnetworkapp.dto.request.post.PostPageReq;
import com.application.socialnetworkapp.service.post.PostServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/user")
@RequiredArgsConstructor
public class PostUserController {

    private final PostServiceImpl postService;

    @PostMapping("/create/post")
    public ResponseEntity<Object> createPost(@RequestBody AddPostRequest request){
      return postService.createPost(request);
    }
    @PostMapping("/list/user/post")
    public ResponseEntity<Object> listPostUser(@RequestBody PostPageReq request){
        return postService.listPostUser(request);
    }

}
