package com.application.socialnetworkapp.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/api/v1/admin")
@RestController
public class HomeController {
    @GetMapping("")
    public ResponseEntity<Object> getHome(){
        return ResponseEntity.status(HttpStatus.OK).body("Welcome");
    }
}
