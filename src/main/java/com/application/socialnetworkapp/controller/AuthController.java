package com.application.socialnetworkapp.controller;

import com.application.socialnetworkapp.dto.request.user.LoginDto;
import com.application.socialnetworkapp.dto.request.user.RegisterDto;
import com.application.socialnetworkapp.service.auth.AuthServiceImpl;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/auth")
public class AuthController {

    @Autowired
    AuthServiceImpl authService;

    @PostMapping("/signup")
    public ResponseEntity<Object> register( @Valid @RequestBody RegisterDto registerDto){
        return  authService.register(registerDto);
    }
    @PostMapping("/signin")
    public ResponseEntity<Object> login( @Valid @RequestBody LoginDto request){
        return  authService.login(request);
    }

}
