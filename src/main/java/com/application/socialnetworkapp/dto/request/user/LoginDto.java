package com.application.socialnetworkapp.dto.request.user;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class LoginDto {
private String email;
private String username;
private String password;
}
