package com.application.socialnetworkapp.dto.request.post;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class PostPageReq {

private int offset;
private int limit;

}
