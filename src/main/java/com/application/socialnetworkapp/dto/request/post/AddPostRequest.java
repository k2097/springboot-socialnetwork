package com.application.socialnetworkapp.dto.request.post;

import com.application.socialnetworkapp.entity.User;
import com.application.socialnetworkapp.util.UserAuth;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;

import java.sql.Blob;
import java.util.UUID;

@Setter
@Getter

public class AddPostRequest {



    private String id;
    private String caption;
    private String hastag;
    private String photo;

    public AddPostRequest(){
        id = UUID.randomUUID().toString();
    }

}
