package com.application.socialnetworkapp.dto.request.user;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.time.LocalDateTime;
import java.util.UUID;

@Setter
@Getter
public class RegisterDto {

  PasswordEncoder passwordEncoder;

private String id;
@NotNull(message = "firstname not null")
private String firstname;

@NotNull(message = "lastname not null")
private String lastname;

@NotNull(message = "username not null")
private String username;

@Email(message = "Email should be valid")
private String email;

private LocalDateTime createdAt;
private LocalDateTime updatedAt;

    @NotNull(message = "password not null")
    @Size(min = 8, message = "password min 8 character")
    private String password;
    private String role;

    public RegisterDto(){
        id = UUID.randomUUID().toString();
    }




}
