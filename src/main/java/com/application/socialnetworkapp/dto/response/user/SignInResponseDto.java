package com.application.socialnetworkapp.dto.response.user;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
public class SignInResponseDto {
    private String username;
    private String role;
    private String token;
}
