package com.application.socialnetworkapp.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.sql.Blob;

@Entity
@Table(name = "post")
@Setter
@Getter
public class Post extends BaseEntity {
    @Id
    @Column(name = "id")
    private String id;
    @Column(name = "caption",columnDefinition = "TEXT")
    private String caption;
    @Column(name = "hastag")
    private String hasTag;
    @Lob
    @Basic(fetch = FetchType.LAZY)
    @Column(length = 10485760)
    private byte[] photo;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;
}
