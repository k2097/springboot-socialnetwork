package com.application.socialnetworkapp.entity;

public enum Role {
    USER,
    ADMIN
}
