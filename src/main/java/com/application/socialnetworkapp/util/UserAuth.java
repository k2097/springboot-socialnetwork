package com.application.socialnetworkapp.util;

import com.application.socialnetworkapp.entity.User;
import com.application.socialnetworkapp.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Component
public class UserAuth {

    @Autowired
    private UserRepository userRepository;
    public User getUserLogin(){
        var auth = (Authentication) SecurityContextHolder.getContext().getAuthentication();
        var name = auth.getName();
        System.out.println(name + "===================== name=====================");
        var isAuth = userRepository.findByUsername(name).orElseThrow();
        return Objects.requireNonNullElseGet(isAuth, () -> {
            return isAuth;
        });
    }

}
