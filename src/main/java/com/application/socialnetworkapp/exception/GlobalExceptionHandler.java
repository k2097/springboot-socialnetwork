package com.application.socialnetworkapp.exception;

import com.application.socialnetworkapp.exception.response.ResponseHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.validation.BindException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;


@ControllerAdvice
@RestControllerAdvice
public class GlobalExceptionHandler {
    private static final Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    @ExceptionHandler(BadCredentialsException.class)
    public ResponseEntity<Object> handleBadCredentialsException(BadCredentialsException ex) {
        return ResponseHandler.generateResponseError(HttpStatus.UNAUTHORIZED,
                ex.getMessage(),
                "Invalid username or password.");
    }


    @ExceptionHandler(ResourceNotFoundException.class)
    public ResponseEntity<Object> handleResourceNotFoundException(ResourceNotFoundException ex) {
        List<String> body = new ArrayList<>();
        body.add(ex.getMessage());
        logger.error("Error Resource Not Found: ", ex);
        return ResponseHandler.generateResponseError(HttpStatus.NOT_FOUND, body,"Resource Not Found");
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex) {
        List<String> errors = ex.getBindingResult().getFieldErrors().stream()
                .map(error -> error.getField() + " : " + error.getDefaultMessage()).toList();
        logger.error("Error Method Argument Not Valid: ", ex);
        return ResponseHandler.generateResponseError(HttpStatus.BAD_REQUEST, errors,"Validation error");
    }

    @ExceptionHandler(BindException.class)
    public ResponseEntity<Object> handleBindException(BindException ex) {
        List<String> errors = ex.getBindingResult().getFieldErrors().stream()
                .map(error -> error.getField() + " : " + error.getDefaultMessage()).toList();
        logger.error("Error Bind Exception: ", ex);
        return ResponseHandler.generateResponseError(HttpStatus.BAD_REQUEST,errors,"Error");
    }

    @ExceptionHandler(AuthenticationException.class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    public ResponseEntity<Object> handleAuthenticationException(AuthenticationException ex) {
        return ResponseHandler.generateResponseError(HttpStatus.BAD_REQUEST,ex.getMessage(),"Error");
    }

}

